@echo off

if "%~1"=="x64" (
    goto :main
)
if "%~1"=="x86" (
    goto :main
) else (
    echo Usage: deploy-cmd.bat [x86^|x64]
    goto :eof
)

:main
setlocal ENABLEEXTENSIONS

set KEY_NAME="HKEY_LOCAL_MACHINE\SOFTWARE\BlenderFoundation"
set VALUE_NAME=ShortVersion

for /F "usebackq skip=2 tokens=1-3" %%A IN (`REG QUERY %KEY_NAME% /v %VALUE_NAME% 2^>nul`) DO (
    set ValueName=%%A
    set ValueType=%%B
    set ValueValue=%%C
)

set WordSize=%~1
set BlendMotionDir="%APPDATA%\Blender Foundation\Blender\%ValueValue%\scripts\addons\BlendMotion"

if defined ValueName (
    mkdir %BlendMotionDir%
    copy src\__init__.py %BlendMotionDir%
    copy src\Core.py %BlendMotionDir%
    copy lib\Leap.py %BlendMotionDir%
    copy lib\Windows\%WordSize%\Leap.dll %BlendMotionDir%
    copy lib\Windows\%WordSize%\LeapPython.pyd %BlendMotionDir%
) else (
    echo Blender not found. Please ^(re^)install it.
)
endlocal

:eof