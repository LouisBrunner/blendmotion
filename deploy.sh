#!/bin/sh

# ajust udev permissions by creating the file /usr/lib/udev/rules.d/10-leap.rules with the following content :
# SUBSYSTEM=="usb",ATTRS{idVendor}=="f182",SYMLINK+="leap_motion",MODE="0666",GROUP="users"
# then reboot

VERSION="`blender --version | head -1 | cut -d " " -f2`"
PATH="/home/$USER/.config/blender/$VERSION/scripts/addons/BlendMotion"

/bin/mkdir -p $PATH
/bin/cp src/__init__.py $PATH
/bin/cp src/Core.py $PATH
/bin/cp lib/Leap.py $PATH

PYTHONPATH=$PYTHONPATH:`pwd`/lib/Linux/x64/ LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib/Leap/ /usr/bin/blender
