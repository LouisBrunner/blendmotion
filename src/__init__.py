#!BPY

bl_info = {
    "name": "BlendMotion",
    "author": "Louis Brunner, Jonathan Nau, Etienne Fesser, Jordane Gori",
    "version": (0, 1),
    "blender": (2, 60, 0),
    "location": "",
    "description": "Bindings for LeapMotion's Leap device",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "System"}

# Hotfix for Blender/Python weird path handling
import sys, os
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
# /Hotfix for Blender/Python weird path handling

import Core, imp
    
imp.reload(Core)

def register():
    Core.register()

def unregister():
    Core.unregister()
	
if __name__ == "__main__":
    register()
    unregister()
