import sys, mathutils, math
import Leap, bpy
from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture

class BlenderLogger:
    is_debug = True

    @staticmethod
    def debug(str):
        if BlenderLogger.is_debug:
            print(str)


class BlenderAPI:
    types = [
                ('Plane', bpy.ops.mesh.primitive_plane_add),
                ('Cube', bpy.ops.mesh.primitive_cube_add),
                ('Circle', bpy.ops.mesh.primitive_circle_add),
                ('Sphere', bpy.ops.mesh.primitive_uv_sphere_add),
                ('Icosphere', bpy.ops.mesh.primitive_ico_sphere_add),
                ('Cylinder', bpy.ops.mesh.primitive_cylinder_add),
                #('Cone', bpy.ops.mesh.primitive_cone_add)
            ]

    def change_object(self, f):
        li = bpy.context.scene.objects.values()
        for idx, val in enumerate(li):
            if val == bpy.context.scene.objects.active:
                old = val
                new = f(li, idx)
                old.select = False
                new.select = True
                bpy.context.scene.objects.active = new
                #BlenderLogger.debug("[GESTURE] Circle-action: old=%s ; new=%s ; clockwiseness=%r" % (str(old), (bpy.context.scene.objects.active), clockwiseness))
                return

    def prev_in_list(self, li, idx):
        return li[(idx - 1) % len(li)]
        
    def next_in_list(self, li, idx):
        return li[(idx + 1) % len(li)]
                
    def prev_object(self):
        self.change_object(self.prev_in_list)

    def next_object(self):
        self.change_object(self.next_in_list)
        
    def move_object(self, matrix):
        bpy.context.scene.objects.active.matrix_local *= matrix
        
    def move_view(self, matrix):
        # TODO: get active screen/area/data
        for screen in bpy.data.screens.values():
            for area in screen.areas.values():
                #space = bpy.types.Context.space_data
                space = area.spaces.active
                if space != None and space.type == "VIEW_3D":
                    space.region_3d.view_matrix *= matrix
    
    def add_object(self):
        bpy.ops.mesh.primitive_cube_add(location=(0.0, 0.0, 0.0))
        #bpy.context.scene.objects.active.data.materials.append(bpy.data.materials[0])
            
    def delete_object(self):
        is_next = False
        next = None
        for item in bpy.context.scene.objects.values():
            if item == bpy.context.scene.objects.active:
                is_next = True
            elif is_next:
                next = item
        if next == None and len(bpy.context.scene.objects.values()) > 0:
            next = bpy.context.scene.objects.values()[0]
        if next != None:
            bpy.context.scene.objects.unlink(bpy.context.scene.objects.active)
            next.select = True
            bpy.context.scene.objects.active = next
            
    def get_radius(self, obj):
        try:
            vec = obj.scale
            res = (vec.x + vec.y + vec.z) / 3
            return res
        except AttributeError:
            return None
            
    def create_object(self, f, loc, rot, radius):
        if radius == None:
            f(location=loc, rotation=rot)
        else:
            try:
                f(location=loc, rotation=rot, radius=radius)
            except TypeError:
                try:
                    f(location=loc, rotation=rot, size=radius)
                except TypeError:
                    return
            
    def morph_object(self, f):
        type = bpy.context.scene.objects.active.name.split('.')[0]
        
        li = BlenderAPI.types
        for idx, key in enumerate(li):
            if key[0] == type:
                obj = bpy.context.scene.objects.active
                loc = obj.location
                rrot = obj.rotation_euler
                rot = (rrot.x, rrot.y, rrot.z)
                radius = self.get_radius(obj)
                bpy.context.scene.objects.unlink(obj)
                self.create_object(f(li, idx)[1], loc, rot, radius)
                return

    def lmorph_object(self):
        self.morph_object(self.prev_in_list)

    def rmorph_object(self):
        self.morph_object(self.next_in_list)

class BlenderListener(Leap.Listener):
    needed_prob = 0.85
    translation_precision = 0.01
    number_fingers = -1
    next_number_fingers = -1
    tmp_fingers = 5
    
    class FistStatus:
        UNLOCKED = 0
        LOCKING = 1
        LOCKED = 2
        UNLOCKING = 3

    def on_init(self, controller):
        BlenderLogger.debug("[LEAP] OnInit")
        
        self.blender = BlenderAPI()
        
        self.edit_view = True
        
        self.circle_count = 0
        
        self.fist_status = BlenderListener.FistStatus.LOCKED
        self.fist_frame = 0
        
        self.swipe_progress = 0
        self.swipe_enabled = True

    def on_connect(self, controller):
        BlenderLogger.debug("[LEAP] OnConnect")
        
        controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE)
        controller.enable_gesture(Leap.Gesture.TYPE_SCREEN_TAP)
        controller.enable_gesture(Leap.Gesture.TYPE_KEY_TAP)
        controller.enable_gesture(Leap.Gesture.TYPE_SWIPE)

    def on_disconnect(self, controller):
        BlenderLogger.debug("[LEAP] OnDisconnect")
        
        pass

    def on_exit(self, controller):
        BlenderLogger.debug("[LEAP] OnExit")

        pass

    def check_fist_status(self, frame):
        if (not frame.hands.is_empty) and frame.pointables.is_empty:
            self.fist_frame += 1
            if self.fist_frame < 60:
                return
            if self.fist_status == BlenderListener.FistStatus.UNLOCKED:
                self.fist_status = BlenderListener.FistStatus.LOCKING
                BlenderLogger.debug("[FIST] Locking")
            elif self.fist_status == BlenderListener.FistStatus.LOCKED:
                self.fist_status = BlenderListener.FistStatus.UNLOCKING
                BlenderLogger.debug("[FIST] Unlocking")
        elif len(frame.pointables) >= 2 and (self.fist_status == BlenderListener.FistStatus.LOCKING or self.fist_status == BlenderListener.FistStatus.UNLOCKING):
            self.fist_frame = 0
            if self.fist_status == BlenderListener.FistStatus.LOCKING:
                self.fist_status = BlenderListener.FistStatus.LOCKED
                BlenderLogger.debug("[FIST] Locked")
            elif self.fist_status == BlenderListener.FistStatus.UNLOCKING:
                self.fist_status = BlenderListener.FistStatus.UNLOCKED
                BlenderLogger.debug("[FIST] Unlocked")
        elif frame.hands.is_empty or len(frame.pointables) > 1:
            self.fist_frame = 0

    def on_frame(self, controller):
        # BlenderLogger.debug("[LEAP] OnFrame")
        
        frame = controller.frame()
        ref_frame = controller.frame(1)

        # BlenderLogger.debug(bpy.context.scene.objects.active.name)
        # BlenderLogger.debug("Frame id: %d, timestamp: %d, hands: %d, fingers: %d, tools: %d, gestures: %d" % (frame.id, frame.timestamp, len(frame.hands), len(frame.pointables), len(frame.tools), len(frame.gestures())))
        
        self.check_fist_status(frame)
       
        if self.fist_status == BlenderListener.FistStatus.UNLOCKED:
            if len(frame.hands) == 1 or len(frame.hands) == 2:
                self.move_object(frame, ref_frame)
        else:
            if len(frame.pointables) == 1 or len(frame.pointables) == 2:
                self.check_gesture(frame, ref_frame)
    
    def check_gesture(self, frame, ref_frame):
        for gesture in frame.gestures():
            if gesture.type == Leap.Gesture.TYPE_CIRCLE:
                circle = CircleGesture(gesture)

                if circle.state == Leap.Gesture.STATE_START:
                    self.circle_count = 0
                if circle.pointable.direction.angle_to(circle.normal) <= Leap.PI / 4:
                    clockwiseness = True
                else:
                    clockwiseness = False

                if circle.state != Leap.Gesture.STATE_START:
                    previous_update = CircleGesture(ref_frame.gesture(circle.id))
                    BlenderLogger.debug("[GESTURE] Circle-progress: old=%f ; new=%f ; clockwiseness=%r ; action=%r" % (previous_update.progress, circle.progress, clockwiseness, self.circle_count + 1 < round(circle.progress)))
                    if self.circle_count + 1 < round(circle.progress):
                        self.circle_count = round(circle.progress)
                        if clockwiseness:
                            self.blender.next_object()
                        else:
                            self.blender.prev_object()
                return True

            elif gesture.type == Leap.Gesture.TYPE_SCREEN_TAP:
                screentap = ScreenTapGesture(gesture)
                self.edit_view = not self.edit_view
                BlenderLogger.debug("[GESTURE] Screen tap action (edit mode: %r)" % self.edit_view)
                return True
                
            elif gesture.type == Leap.Gesture.TYPE_KEY_TAP:
                keytap = KeyTapGesture(gesture)
                BlenderLogger.debug("[GESTURE] Key tap action (fingers: %r)" % len(frame.pointables))
                if len(frame.pointables) == 1:
                    self.blender.add_object()
                else:
                    self.blender.delete_object()
                return True
                
            elif gesture.type == Leap.Gesture.TYPE_SWIPE:
                swipe = SwipeGesture(gesture)
                swipe_len = math.fabs(swipe.start_position.x - swipe.position.x)
                BlenderLogger.debug("[GESTURE] Swipe action (size: %f)" % swipe_len)
                if swipe_len < self.swipe_progress:
                    self.swipe_enabled = True
                else:
                    self.swipe_progress = swipe_len
                if self.swipe_enabled:
                    if swipe.direction.x > 0:
                        BlenderLogger.debug("[GESTURE] Swipe left-to-right")
                        self.blender.rmorph_object()
                    else:
                        BlenderLogger.debug("[GESTURE] Swipe right-to-left")
                        self.blender.lmorph_object()
                    self.swipe_progress = swipe_len
                    self.swipe_enabled = False
                return True
#                BlenderLogger.debug( "Swipe id: %d, state: %s, position: %s, direction: %s, speed: %f" % (
#                        gesture.id, self.state_string(gesture.state),
#                        swipe.position, swipe.direction, swipe.speed))
                return True
        return False

    def state_string(self, state):
        if state == Leap.Gesture.STATE_START:
            return "STATE_START"

        if state == Leap.Gesture.STATE_UPDATE:
            return "STATE_UPDATE"

        if state == Leap.Gesture.STATE_STOP:
            return "STATE_STOP"

        if state == Leap.Gesture.STATE_INVALID:
            return "STATE_INVALID"

    def move_object(self, frame, ref_frame):
        # TODO: Adapte the axis based on the camera -> GLHF

        if len(frame.hands) == 2:
            if BlenderListener.number_fingers == -1:
                # first frame
                BlenderListener.number_fingers = len(frame.hands.leftmost.fingers)
            elif BlenderListener.number_fingers != len(frame.hands.leftmost.fingers) and BlenderListener.next_number_fingers != len(frame.hands.leftmost.fingers):
                # to change next state fingers
                BlenderListener.next_number_fingers = len(frame.hands.leftmost.fingers)
                BlenderListener.tmp_fingers = 5
            elif BlenderListener.next_number_fingers == len(frame.hands.leftmost.fingers) and BlenderListener.next_number_fingers == len(ref_frame.hands.leftmost.fingers):
                BlenderListener.tmp_fingers -= 1
        
        if BlenderListener.tmp_fingers == 0:
            BlenderListener.number_fingers = BlenderListener.next_number_fingers
        
        matrix = mathutils.Matrix()
        matrix.identity()
        BlenderLogger.debug("Number hands %i" % (len(frame.hands)))
        BlenderLogger.debug("[VECTOR] Numbers fingers left hand: %i" % (BlenderListener.number_fingers))
        BlenderLogger.debug("[MATRIX] Rotation probability: %f" % (frame.rotation_probability(ref_frame)))
        if frame.rotation_probability(ref_frame) > BlenderListener.needed_prob:
            if len(frame.hands) == 1 or BlenderListener.number_fingers == 1:
                matrix *= mathutils.Matrix.Rotation(-frame.rotation_angle(ref_frame, Leap.Vector(1, 0, 0)), 4, 'Y')
            elif len(frame.hands) == 1 or BlenderListener.number_fingers == 2:
                matrix *= mathutils.Matrix.Rotation(-frame.rotation_angle(ref_frame, Leap.Vector(0, 1, 0)), 4, 'Z')
            elif len(frame.hands) == 1 or BlenderListener.number_fingers == 3:
                matrix *= mathutils.Matrix.Rotation(-frame.rotation_angle(ref_frame, Leap.Vector(0, 0, 1)), 4, 'X')

        BlenderLogger.debug("[MATRIX] Translation probability: %f" % (frame.translation_probability(ref_frame)))
        if frame.translation_probability(ref_frame) > BlenderListener.needed_prob:
            if len(frame.hands) == 1:
                vector = mathutils.Vector((frame.translation(ref_frame).z, frame.translation(ref_frame).x, frame.translation(ref_frame).y))
            else:
                if BlenderListener.number_fingers == 1:
                    vector = mathutils.Vector((frame.translation(ref_frame).z, 0, 0))
                elif BlenderListener.number_fingers == 2:
                    vector = mathutils.Vector((0, frame.translation(ref_frame).x, 0))
                elif BlenderListener.number_fingers == 3:
                    vector = mathutils.Vector((0, 0, frame.translation(ref_frame).y))            
                else:
                    vector = mathutils.Vector((0, 0, 0))
            matrix *= mathutils.Matrix.Translation(vector * BlenderListener.translation_precision)

        BlenderLogger.debug("[MATRIX] Scale probability: %f" % (frame.scale_probability(ref_frame)))
        if frame.scale_probability(ref_frame) > BlenderListener.needed_prob and len(frame.pointables) == 2:
            matrix *= mathutils.Matrix.Scale(frame.scale_factor(ref_frame), 4)
        
        if self.edit_view:
            self.blender.move_view(matrix)
        else:
            self.blender.move_object(matrix)


def init():
    BlenderLogger.debug("[BLENDER] Init")
    
    global listener
    global controller
    
    listener = None
    controller = None

def register():
    BlenderLogger.debug("[BLENDER] Register")
    
    global listener
    global controller

    if listener == None and controller == None:
        listener = BlenderListener()
        controller = Leap.Controller()
        
        controller.add_listener(listener)

def unregister():
    BlenderLogger.debug("[BLENDER] Unregister")
    
    global listener
    global controller

    if listener != None and controller != None:
        controller.remove_listener(listener)
        listener = None
        controller = None

init()
if __name__ == "__main__":
    register()
    unregister()
